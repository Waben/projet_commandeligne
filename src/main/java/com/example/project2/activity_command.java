package com.example.project2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.project2.tableClass.Command;
import com.example.project2.tableClass.Product;

import java.util.ArrayList;
import java.util.List;

public class activity_command extends AppCompatActivity {


    class ProductList  extends BaseAdapter {
        List<Product> dataSource;

        public ProductList(List<Product> data) {
            //super(MainActivity.this, R.layout.row, data);
            dataSource=data;
        }
        public void setData(List<Product> d)
        {
            dataSource=d;
        }
        @Override
        public int getCount() {
            return dataSource.size();
        }
        @Override
        public Object getItem(int position) {
            return dataSource.get(position);
        }
        @Override
        public long getItemId(int position) {
            return position;
        }
        public void updateCityList(List<Product> newlist) {
            dataSource.clear();
            dataSource.addAll(newlist);
            this.notifyDataSetChanged();
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Product c = dataSource.get(position);
            convertView = LayoutInflater.from(activity_command.this).inflate(R.layout.activity_command_item_product,null);

            TextView Text1 = (TextView) convertView.findViewById(R.id.textView5);

            Text1.setText("Product "+position);

            return convertView;
        }
    }
    ProductList PL=null;


    private static final String TAG = com.example.project2.tableClass.Command.class.getSimpleName();
    Command command;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_command);

        command = (Command) getIntent().getParcelableExtra(activity_command.TAG);
        TextView commandName = (TextView) findViewById(R.id.textView12);
        TextView lastName = (TextView) findViewById(R.id.textView13);
        TextView firstName = (TextView) findViewById(R.id.textView14);
        TextView address = (TextView) findViewById(R.id.textView21);
        TextView price = (TextView) findViewById(R.id.textView20);
        TextView status = (TextView) findViewById(R.id.textView19);













        ArrayList<Product> ArrOff=new ArrayList<>();
        ArrOff.add(new Product(5, "lol", "lol", 56, 1, 56, 56));
        ArrOff.add(new Product(5, "lol", "lol", 56, 1, 56, 56));
        ArrOff.add(new Product(5, "lol", "lol", 56, 1, 56, 56));
        ArrOff.add(new Product(5, "lol", "lol", 56, 1, 56, 56));
        PL=new activity_command.ProductList(ArrOff);

        ListView list = (ListView) findViewById(R.id.listview);
        list.setAdapter(PL);

    }
}