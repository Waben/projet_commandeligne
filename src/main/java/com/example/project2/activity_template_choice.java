package com.example.project2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class activity_template_choice extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_template_choice);

        final ImageView imageView = findViewById(R.id.imageView);
        imageView.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                final Intent intent = new Intent(getApplicationContext(), activity_configure_template.class);


               /* final City item = (City) parent.getItemAtPosition(position);
                intent.putExtra(City.TAG, item );*/

                Bundle B=new Bundle();
                B.putInt("nb",1);
                intent.putExtra("data",B);


                startActivityForResult(intent,1);
            }
        });

        final ImageView imageView1 = findViewById(R.id.imageView4);
        imageView1.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                final Intent intent = new Intent(getApplicationContext(), activity_configure_template.class);


               /* final City item = (City) parent.getItemAtPosition(position);
                intent.putExtra(City.TAG, item );*/

                Bundle B=new Bundle();
                B.putInt("nb",2);
                intent.putExtra("data",B);


                startActivityForResult(intent,1);
            }
        });

        final ImageView imageView2 = findViewById(R.id.imageView5);
        imageView2.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                final Intent intent = new Intent(getApplicationContext(), activity_configure_template.class);


               /* final City item = (City) parent.getItemAtPosition(position);
                intent.putExtra(City.TAG, item );*/

                Bundle B=new Bundle();
                B.putInt("nb",3);
                intent.putExtra("data",B);


                startActivityForResult(intent,1);
            }
        });
    }
}