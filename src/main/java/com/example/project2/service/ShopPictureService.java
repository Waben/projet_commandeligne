package com.example.project2.service;

import android.app.Activity;
import android.util.Log;

import androidx.annotation.NonNull;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.project2.tableClass.ShopPicture;
import com.example.project2.tableClass.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShopPictureService {
    private static final String TAG = ClientService.class.getSimpleName();

    //private static final String URL = "http://192.168.0.35:5000/"; // vitrolles
   // private static final String URL = "http://192.168.137.1:5000/"; // cite u
    private static final String CURRENT_TAG = "CURRENT_PICTURE_SHOP";
    private static String URL;
    private RequestQueue queue;

    public ShopPictureService(@NonNull final Activity activity, String url) {
        queue = Volley.newRequestQueue(activity.getApplicationContext());
        URL = url;
    }

    private List<Table> JsonToShopList(JSONObject response)throws Exception{
        final JSONArray userJson = response.getJSONArray("images_boutiques");
        List<Table> pictures = new ArrayList<>();
        for (int i = 0; i < userJson.length() ; i++) {
            final JSONObject premier = userJson.getJSONObject(i);
            final int id = premier.getInt("id");
            final String name = premier.getString("name");
            final String location = premier.getString("emplacement");
            final int shopId = premier.getInt("boutique_id");
            final int idTemplate = premier.getInt("id_template");
            ShopPicture shopPicture = new ShopPicture(id, name, location, shopId, idTemplate);
            pictures.add(shopPicture);
        }
        return pictures;
    }

    private List<Table> JsonToShop(JSONObject response) throws Exception{
        List<Table> pictures = new ArrayList<>();
        final int id = response.getInt("id");
        final String name = response.getString("name");
        final String location = response.getString("emplacement");
        final int shopId = response.getInt("boutique_id");
        final int idTemplate = response.getInt("id_template");
        ShopPicture shopPicture = new ShopPicture(id, name, location, shopId, idTemplate);
        pictures.add(shopPicture);
        return pictures;
    }

    public void getShopPicture(@NonNull final int index, @NonNull final CallBack callback) {
        final String url;
        if(index == -1)
            url = String.format("%s/api/image_boutique", URL);
        else
            url = String.format("%s/api/image_boutique/" + index, URL);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","GET " + response);
                            if(index == -1)
                                callback.onUpdtate(JsonToShopList(response));
                            else
                                callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        callback.onError(error);

                    }
                });
        queue.add(jsonObjectRequest);
    }

    public void postShopPicture(@NonNull final ShopPicture shopPicture, @NonNull final CallBack callback) {
        final String url = String.format("%s/api/image_boutique", URL);
        JSONObject jsonParams = new JSONObject();
        try{
            jsonParams.put("name", shopPicture.getName());
            jsonParams.put("emplacement", shopPicture.getLocation());
            jsonParams.put("boutique_id", shopPicture.getShopId());
            jsonParams.put("id_template", shopPicture.getIdTemplate());
        }catch (JSONException e){
            e.printStackTrace();
        }
        //////////////////////
        Log.d(TAG,"Json:"+ jsonParams);
        //////////////////////////
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, jsonParams, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","POST " + response);

                            callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            System.out.println("error try");
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String,String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        queue.add(jsonObjectRequest);
    }

    public void putShopPicture(@NonNull ShopPicture shopPicture, @NonNull final CallBack callback) {
        final String url = String.format("%s/api/image_boutique/" + shopPicture.getId(), URL);
        JSONObject jsonParams = new JSONObject();
        try{
            if(shopPicture.getName() != null)
                jsonParams.put("name", shopPicture.getName());
            if(shopPicture.getLocation() != null)
                jsonParams.put("emplacement", shopPicture.getLocation());
            if(shopPicture.getShopId() > 0)
                jsonParams.put("boutique_id", shopPicture.getShopId());
            if(shopPicture.getIdTemplate() > 0)
                jsonParams.put("id_template", shopPicture.getIdTemplate());
        }catch (JSONException e){
            e.printStackTrace();
        }
//////////////////////////////////////////////////////
        Log.d("BEFORE","Json:"+ jsonParams);
        ////////////////////////////////////////////////////
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.PUT, url, jsonParams, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","PUT " + response);
                            callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            System.out.println("error try");
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String,String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        queue.add(jsonObjectRequest);
    }

    public void deleteShopPicture(@NonNull final int id, @NonNull final CallBack callback) {
        final String url = String.format("%s/api/image_boutique/"+id, URL);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.DELETE, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","ERROR " + response);
                            callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        callback.onError(error);

                    }
                });
        queue.add(jsonObjectRequest);
    }

    public void cancel() {
        queue.cancelAll(CURRENT_TAG);
    }
}
