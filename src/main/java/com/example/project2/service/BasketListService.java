package com.example.project2.service;

import android.app.Activity;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.project2.tableClass.BasketList;
import com.example.project2.tableClass.Client;
import com.example.project2.tableClass.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BasketListService {
    private static final String TAG = BasketListService.class.getSimpleName();

    //private static final String URL = "http://192.168.0.35:5000/"; // vitrolles
    //private static final String URL = "http://192.168.137.1:5000/"; // cite u
    private static String URL;
    private static final String CURRENT_TAG = "CURRENT_CLIENT";

    private RequestQueue queue;

    public BasketListService(@NonNull final Activity activity, String url) {
        queue = Volley.newRequestQueue(activity.getApplicationContext());
        URL = url;
    }

    private List<Table> JsonToShopList(JSONObject response)throws Exception{
        final JSONArray userJson = response.getJSONArray("panier_list");
        List<Table> list = new ArrayList<>();
        for (int i = 0; i < userJson.length() ; i++) {
            final JSONObject premier = userJson.getJSONObject(i);
            final int id = premier.getInt("id");
            final int basketId = premier.getInt("id_panier");
            final int quantity = premier.getInt("quantity");
            final int productId = premier.getInt("produit_id");
            BasketList basketList = new BasketList(id,basketId, quantity, productId);
            list.add(basketList);
        }
        return list;
    }

    private List<Table> JsonToShop(JSONObject response) throws Exception{
        List<Table> list = new ArrayList<>();
        final int id = response.getInt("id");
        final int basketId = response.getInt("id_panier");
        final int quantity = response.getInt("quantity");
        final int productId = response.getInt("produit_id");
        BasketList basketList = new BasketList(id,basketId, quantity, productId);
        list.add(basketList);
        return list;
    }

    public void getBasketList(@NonNull final int index, @NonNull final CallBack callback) {
        final String url;
        if(index == -1)
            url = String.format("%s/api/panier_list", URL);
        else
            url = String.format("%s/api/panier_list/" + index, URL);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","GET " + response);
                            if(index == -1)
                                callback.onUpdtate(JsonToShopList(response));
                            else
                                callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        callback.onError(error);

                    }
                });
        queue.add(jsonObjectRequest);
    }

    public void postBasketList(@NonNull final BasketList basketList, @NonNull final CallBack callback) {
        final String url = String.format("%s/api/panier_list", URL);
        JSONObject jsonParams = new JSONObject();
        try{
            jsonParams.put("id_panier", basketList.getBasketId());
            jsonParams.put("quantity", basketList.getQuantity());
            jsonParams.put("produit_id", basketList.getProductId());
        }catch (JSONException e){
            e.printStackTrace();
        }
        //////////////////////
        Log.d(TAG,"Json:"+ jsonParams);
        //////////////////////////
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, jsonParams, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","POST " + response);

                            callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            System.out.println("error try");
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String,String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        queue.add(jsonObjectRequest);
    }

    public void putBasketList(@NonNull BasketList basketList, @NonNull final CallBack callback) {
        final String url = String.format("%s/api/panier_list/" + basketList.getId(), URL);
        JSONObject jsonParams = new JSONObject();
        try{
            if(basketList.getBasketId() > 0)
                jsonParams.put("id_panier", basketList.getBasketId());
            if(basketList.getQuantity() > 0)
                jsonParams.put("quantity", basketList.getQuantity());
            if(basketList.getProductId() > 0)
                jsonParams.put("produit_id", basketList.getProductId());
        }catch (JSONException e){
            e.printStackTrace();
        }
//////////////////////////////////////////////////////
        Log.d("BEFORE","Json:"+ jsonParams);
        ////////////////////////////////////////////////////
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.PUT, url, jsonParams, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","PUT " + response);
                            callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            System.out.println("error try");
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String,String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        queue.add(jsonObjectRequest);
    }

    public void deleteBasketList(@NonNull final int id, @NonNull final CallBack callback) {
        final String url = String.format("%s/api/panier_list/"+id, URL);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.DELETE, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","ERROR " + response);
                            callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        callback.onError(error);

                    }
                });
        queue.add(jsonObjectRequest);
    }

    public void cancel() {
        queue.cancelAll(CURRENT_TAG);
    }
}
