package com.example.project2.service;

import android.app.Activity;
import android.telecom.Call;
import android.util.Log;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.project2.tableClass.Shop;
import com.example.project2.tableClass.Table;
import com.example.project2.User;
import com.example.project2.UserService;
import com.google.android.material.tabs.TabLayout;

import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShopService {
    private static final String TAG = ShopService.class.getSimpleName();

    //private static final String URL = "http://192.168.0.35:5000/"; // vitrolles
    //private static final String URL = "http://192.168.137.1:5000/"; // cite u
    private static final String CURRENT_TAG = "CURRENT_SHOP";
    private static String URL;
    private RequestQueue queue;

    public ShopService(@NonNull final Activity activity, String url) {
        queue = Volley.newRequestQueue(activity.getApplicationContext());
        URL = url;
    }

    private List<Table> JsonToShopList(JSONObject response)throws Exception{
        final JSONArray userJson = response.getJSONArray("boutiques");
        List<Table> shops = new ArrayList<>();
        for (int i = 0; i < userJson.length() ; i++) {
            final JSONObject premier = userJson.getJSONObject(i);
            final int id = premier.getInt("id");
            final String shopName = premier.getString("nom_boutique");
            final String ownerSurname = premier.getString("prenom_proprio");
            final String ownerName = premier.getString("nom_proprio");
            final String siret = premier.getString("siret");
            final String addresse = premier.getString("addresse");
            final String mail = premier.getString("mail");
            final String contact = premier.getString("coordonnes");
            Shop shop = new Shop(id,shopName,ownerName,ownerSurname,siret,addresse,mail,contact);
            shops.add(shop);
        }
        return shops;
    }

    private List<Table> JsonToShop(JSONObject response) throws Exception{
        List<Table> shops = new ArrayList<>();
        final int id = response.getInt("id");
        final String shopName = response.getString("nom_boutique");
        final String ownerSurname = response.getString("prenom_proprio");
        final String ownerName = response.getString("nom_proprio");
        final String siret = response.getString("siret");
        final String addresse = response.getString("addresse");
        final String mail = response.getString("mail");
        final String contact = response.getString("coordonnes");
        Shop shop = new Shop(id,shopName,ownerName,ownerSurname,siret,addresse,mail,contact);
        shops.add(shop);
        return shops;
    }

    public void getShop(@NonNull final int index, @NonNull final CallBack callback) {
        final String url;
        if(index == -1)
            url = String.format("%s/api/boutiques", URL);
        else
            url = String.format("%s/api/boutiques/" + index, URL);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","GET " + response);
                            if(index == -1)
                                callback.onUpdtate(JsonToShopList(response));
                            else
                                callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        callback.onError(error);

                    }
                });
        queue.add(jsonObjectRequest);
    }

    public void postShop(@NonNull final Shop shop, @NonNull final CallBack callback) {
        final String url = String.format("%s/api/boutiques", URL);
        JSONObject jsonParams = new JSONObject();
        try{
            jsonParams.put("nom_boutique", shop.getShopName());
            jsonParams.put("prenom_proprio", shop.getOwnerSurname());
            jsonParams.put("nom_proprio", shop.getOwnerName());
            jsonParams.put("siret", shop.getSiret());
            jsonParams.put("mail", shop.getMail());
            jsonParams.put("addresse", shop.getAddresse());
            jsonParams.put("coordonnes", shop.getContact());
        }catch (JSONException e){
            e.printStackTrace();
        }
        //////////////////////
        Log.d(TAG,"Json:"+ jsonParams);
        //////////////////////////
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, jsonParams, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","POST " + response);

                            callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            System.out.println("error try");
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String,String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        queue.add(jsonObjectRequest);
    }

    public void putShop(@NonNull Shop shop, @NonNull final CallBack callback) {
        //a modifier
        final String url = String.format("%s/api/boutiques/" + shop.getId(), URL);
        JSONObject jsonParams = new JSONObject();
        try{
            if(shop.getShopName() != null)
                jsonParams.put("nom_boutique", shop.getShopName());
            if(shop.getOwnerSurname() != null)
                jsonParams.put("prenom_proprio", shop.getOwnerSurname());
            if(shop.getOwnerName() != null)
                jsonParams.put("nom_proprio", shop.getOwnerName());
            if(shop.getSiret() != null)
                jsonParams.put("siret", shop.getSiret());
            if(shop.getMail() != null)
                jsonParams.put("mail", shop.getMail());
            if(shop.getAddresse() != null)
                jsonParams.put("addresse", shop.getAddresse());
            if(shop.getContact() != null)
                jsonParams.put("coordonnes", shop.getContact());
        }catch (JSONException e){
            e.printStackTrace();
        }
//////////////////////////////////////////////////////
        Log.d("BEFORE","Json:"+ jsonParams);
        ////////////////////////////////////////////////////
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.PUT, url, jsonParams, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","PUT " + response);
                            callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            System.out.println("error try");
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String,String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        queue.add(jsonObjectRequest);
    }

    public void deleteShop(@NonNull final int id, @NonNull final CallBack callback) {
        final String url = String.format("%s/api/boutiques/"+id, URL);
        /*
        A tester avec juste l'index
        JSONObject jsonParams = new JSONObject();
        try{
            jsonParams.put("id", id);
            jsonParams.put("name", "blabla");
        }catch (JSONException e){
            e.printStackTrace();
        }*/

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.DELETE, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","ERROR " + response);
                            callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        callback.onError(error);

                    }
                });
        queue.add(jsonObjectRequest);
    }

    public void cancel() {
        queue.cancelAll(CURRENT_TAG);
    }
}
