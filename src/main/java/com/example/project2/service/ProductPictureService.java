package com.example.project2.service;

import android.app.Activity;
import android.util.Log;

import androidx.annotation.NonNull;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.project2.tableClass.Client;
import com.example.project2.tableClass.ProductPicture;
import com.example.project2.tableClass.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductPictureService {

    private static final String TAG = ProductPictureService.class.getSimpleName();

    //private static final String URL = "http://192.168.0.35:5000/"; // vitrolles
    //private static final String URL = "http://192.168.137.1:5000/"; // cite u
    private static final String CURRENT_TAG = "CURRENT_CLIENT";
    private static String URL;
    private RequestQueue queue;

    public ProductPictureService(@NonNull final Activity activity, String url) {
        queue = Volley.newRequestQueue(activity.getApplicationContext());
        URL = url;
    }

    private List<Table> JsonToShopList(JSONObject response)throws Exception{
        final JSONArray userJson = response.getJSONArray("image_prods");
        List<Table> pictures = new ArrayList<>();
        for (int i = 0; i < userJson.length() ; i++) {
            final JSONObject premier = userJson.getJSONObject(i);
            final int id = premier.getInt("id");
            final String name = premier.getString("name");
            final String location = premier.getString("emplacement");
            final int productTypeId = premier.getInt("type_prod_id");
            final int productId = premier.getInt("produit_id");
            ProductPicture productPicture = new ProductPicture(id, name, location, productId, productTypeId);
            pictures.add(productPicture);
        }
        return pictures;
    }

    private List<Table> JsonToShop(JSONObject response) throws Exception{
        List<Table> pictures = new ArrayList<>();
        final int id = response.getInt("id");
        final String name = response.getString("name");
        final String location = response.getString("emplacement");
        final int productTypeId = response.getInt("type_prod_id");
        final int productId = response.getInt("produit_id");
        ProductPicture productPicture = new ProductPicture(id, name, location, productId, productTypeId);
        pictures.add(productPicture);
        return pictures;
    }

    public void getProductPicture(@NonNull final int index, @NonNull final CallBack callback) {
        final String url;
        if(index == -1)
            url = String.format("%s/api/image_prod", URL);
        else
            url = String.format("%s/api/image_prod/" + index, URL);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","GET " + response);
                            if(index == -1)
                                callback.onUpdtate(JsonToShopList(response));
                            else
                                callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        callback.onError(error);

                    }
                });
        queue.add(jsonObjectRequest);
    }

    public void postProductPicture(@NonNull final ProductPicture productPicture, @NonNull final CallBack callback) {
        final String url = String.format("%s/api/image_prod", URL);
        JSONObject jsonParams = new JSONObject();
        try{
            jsonParams.put("name", productPicture.getName());
            jsonParams.put("emplacement", productPicture.getLocation());
            jsonParams.put("type_prod_id", productPicture.getProductTypeId());
            jsonParams.put("produit_id", productPicture.getProductId());
        }catch (JSONException e){
            e.printStackTrace();
        }
        //////////////////////
        Log.d(TAG,"Json:"+ jsonParams);
        //////////////////////////
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, jsonParams, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","POST " + response);

                            callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            System.out.println("error try");
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String,String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        queue.add(jsonObjectRequest);
    }

    public void putProductPicture(@NonNull ProductPicture productPicture, @NonNull final CallBack callback) {
        final String url = String.format("%s/api/image_prod/" + productPicture.getId(), URL);
        JSONObject jsonParams = new JSONObject();
        try{
            if(productPicture.getName() != null)
                jsonParams.put("name", productPicture.getName());
            if(productPicture.getLocation() != null)
                jsonParams.put("emplacement", productPicture.getLocation());
            if(productPicture.getProductId() > 0)
                jsonParams.put("id_panier", productPicture.getProductId());
            if(productPicture.getProductTypeId() > 0)
                jsonParams.put("id_centre_interet", productPicture.getProductTypeId());
        }catch (JSONException e){
            e.printStackTrace();
        }
//////////////////////////////////////////////////////
        Log.d("BEFORE","Json:"+ jsonParams);
        ////////////////////////////////////////////////////
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.PUT, url, jsonParams, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","PUT " + response);
                            callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            System.out.println("error try");
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String,String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        queue.add(jsonObjectRequest);
    }

    public void deleteProductPicture(@NonNull final int id, @NonNull final CallBack callback) {
        final String url = String.format("%s/api/image_prod/"+id, URL);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.DELETE, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","ERROR " + response);
                            callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        callback.onError(error);

                    }
                });
        queue.add(jsonObjectRequest);
    }

    public void cancel() {
        queue.cancelAll(CURRENT_TAG);
    }
}
