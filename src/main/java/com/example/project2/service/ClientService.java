package com.example.project2.service;

import android.app.Activity;
import android.util.Log;

import androidx.annotation.NonNull;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.project2.tableClass.Category;
import com.example.project2.tableClass.Client;
import com.example.project2.tableClass.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClientService {

    private static final String TAG = ClientService.class.getSimpleName();

    //private static final String URL = "http://192.168.0.35:5000/"; // vitrolles
    //private static final String URL = "http://192.168.137.1:5000/"; // cite u
    private static final String CURRENT_TAG = "CURRENT_CLIENT";
    private static String URL;

    private RequestQueue queue;

    public ClientService(@NonNull final Activity activity, String url) {
        queue = Volley.newRequestQueue(activity.getApplicationContext());
        URL = url;
    }

    private List<Table> JsonToShopList(JSONObject response)throws Exception{
        final JSONArray userJson = response.getJSONArray("clients");
        List<Table> clients = new ArrayList<>();
        for (int i = 0; i < userJson.length() ; i++) {
            final JSONObject premier = userJson.getJSONObject(i);
            final int id = premier.getInt("id");
            final String name = premier.getString("name");
            final String surname = premier.getString("surname");
            final String mail = premier.getString("mail");
            final String addresse = premier.getString("addresse");
            final int idBasket = premier.getInt("id_panier");
            final int idHobby = premier.getInt("id_centre_interet");
            Client client = new Client(id, name, surname,mail, addresse, idBasket, idHobby);
            clients.add(client);
        }
        return clients;
    }

    private List<Table> JsonToShop(JSONObject response) throws Exception{
        List<Table> clients = new ArrayList<>();
        final int id = response.getInt("id");
        final String name = response.getString("name");
        final String surname = response.getString("surname");
        final String mail = response.getString("mail");
        final String addresse = response.getString("addresse");
        final int idBasket = response.getInt("id_panier");
        final int idHobby = response.getInt("id_centre_interet");
        Client client = new Client(id, name, surname,mail, addresse, idBasket, idHobby);
        clients.add(client);
        return clients;
    }

    public void getClient(@NonNull final int index, @NonNull final CallBack callback) {
        final String url;
        if(index == -1)
            url = String.format("%s/api/clients", URL);
        else
            url = String.format("%s/api/clients/" + index, URL);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","GET " + response);
                            if(index == -1)
                                callback.onUpdtate(JsonToShopList(response));
                            else
                                callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        callback.onError(error);

                    }
                });
        queue.add(jsonObjectRequest);
    }

    public void postClient(@NonNull final Client client, @NonNull final CallBack callback) {
        final String url = String.format("%s/api/clients", URL);
        JSONObject jsonParams = new JSONObject();
        try{
            jsonParams.put("name", client.getName());
            jsonParams.put("surname", client.getSurname());
            jsonParams.put("mail", client.getMail());
            jsonParams.put("addresse", client.getAddresse());
            jsonParams.put("id_panier", client.getIdBasket());
            jsonParams.put("id_centre_interet", client.getIdHobby());
        }catch (JSONException e){
            e.printStackTrace();
        }
        //////////////////////
        Log.d(TAG,"Json:"+ jsonParams);
        //////////////////////////
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, jsonParams, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","POST " + response);

                            callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            System.out.println("error try");
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String,String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        queue.add(jsonObjectRequest);
    }

    public void putClient(@NonNull Client client, @NonNull final CallBack callback) {
        final String url = String.format("%s/api/clients/" + client.getId(), URL);
        JSONObject jsonParams = new JSONObject();
        try{
            if(client.getName() != null)
                jsonParams.put("name", client.getName());
            if(client.getSurname() != null)
                jsonParams.put("surname", client.getSurname());
            if(client.getMail() != null)
                jsonParams.put("mail", client.getMail());
            if(client.getAddresse() != null)
                jsonParams.put("addresse", client.getAddresse());
            if(client.getIdBasket() > 0)
                jsonParams.put("id_panier", client.getIdBasket());
            if(client.getIdHobby() > 0)
                jsonParams.put("id_centre_interet", client.getIdHobby());
        }catch (JSONException e){
            e.printStackTrace();
        }
//////////////////////////////////////////////////////
        Log.d("BEFORE","Json:"+ jsonParams);
        ////////////////////////////////////////////////////
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.PUT, url, jsonParams, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","PUT " + response);
                            callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            System.out.println("error try");
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String,String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        queue.add(jsonObjectRequest);
    }

    public void deleteClient(@NonNull final int id, @NonNull final CallBack callback) {
        final String url = String.format("%s/api/clients/"+id, URL);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.DELETE, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","ERROR " + response);
                            callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        callback.onError(error);

                    }
                });
        queue.add(jsonObjectRequest);
    }

    public void cancel() {
        queue.cancelAll(CURRENT_TAG);
    }
}
