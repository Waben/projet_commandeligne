package com.example.project2.service;

import android.app.Activity;
import android.util.Log;

import androidx.annotation.NonNull;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.project2.tableClass.Client;
import com.example.project2.tableClass.Correspondence;
import com.example.project2.tableClass.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CorrespondenceService {

    private static final String TAG = CorrespondenceService.class.getSimpleName();

    //private static final String URL = "http://192.168.0.35:5000/"; // vitrolles
    //private static final String URL = "http://192.168.137.1:5000/"; // cite u
    private static final String CURRENT_TAG = "CURRENT_COREESPONDENCE";
    private static String URL;

    private RequestQueue queue;

    public CorrespondenceService(@NonNull final Activity activity, String url) {
        queue = Volley.newRequestQueue(activity.getApplicationContext());
        URL = url;
    }

    private List<Table> JsonToShopList(JSONObject response)throws Exception{
        final JSONArray userJson = response.getJSONArray("correspondances");
        List<Table> correspondences = new ArrayList<>();
        for (int i = 0; i < userJson.length() ; i++) {
            final JSONObject premier = userJson.getJSONObject(i);
            final int id = premier.getInt("id");
            final int hobbyId = premier.getInt("centre_interet_id");
            final int categoryId = premier.getInt("categorie_id");
            Correspondence correspondence = new Correspondence(id,hobbyId, categoryId);
            correspondences.add(correspondence);
        }
        return correspondences;
    }

    private List<Table> JsonToShop(JSONObject response) throws Exception{
        List<Table> correspondences = new ArrayList<>();
        final int id = response.getInt("id");
        final int hobbyId = response.getInt("centre_interet_id");
        final int categoryId = response.getInt("categorie_id");
        Correspondence correspondence = new Correspondence(id,hobbyId, categoryId);
        correspondences.add(correspondence);
        return correspondences;
    }

    public void getCorrespondence(@NonNull final int index, @NonNull final CallBack callback) {
        final String url;
        if(index == -1)
            url = String.format("%s/api/correspondances", URL);
        else
            url = String.format("%s/api/correspondances/" + index, URL);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","GET " + response);
                            if(index == -1)
                                callback.onUpdtate(JsonToShopList(response));
                            else
                                callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        callback.onError(error);

                    }
                });
        queue.add(jsonObjectRequest);
    }

    /**
     * Useless method because arguments are only on fk and can't modify thoses args
     * @param correspondence
     * @param callback
     */

    public void postCorrespondence(@NonNull final Correspondence correspondence, @NonNull final CallBack callback) {
        final String url = String.format("%s/api/correspondances", URL);
        JSONObject jsonParams = new JSONObject();
        try{
            jsonParams.put("categorie_id", correspondence.getCategoryId());
            jsonParams.put("centre_interet_id", correspondence.getHobbyId());
        }catch (JSONException e){
            e.printStackTrace();
        }
        //////////////////////
        Log.d(TAG,"Json:"+ jsonParams);
        //////////////////////////
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, jsonParams, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","POST " + response);

                            callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            System.out.println("error try");
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String,String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        queue.add(jsonObjectRequest);
    }

    public void putCorrespondence(@NonNull Correspondence correspondence, @NonNull final CallBack callback) {
        final String url = String.format("%s/api/correspondances/" + correspondence.getId(), URL);
        JSONObject jsonParams = new JSONObject();
        try{
            if(correspondence.getHobbyId() > 0)
                jsonParams.put("centre_interet_id", correspondence.getHobbyId());
            if(correspondence.getCategoryId() > 0)
                jsonParams.put("categorie_id", correspondence.getCategoryId());
        }catch (JSONException e){
            e.printStackTrace();
        }
//////////////////////////////////////////////////////
        Log.d("BEFORE","Json:"+ jsonParams);
        ////////////////////////////////////////////////////
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.PUT, url, jsonParams, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","PUT " + response);
                            callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            System.out.println("error try");
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String,String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        queue.add(jsonObjectRequest);
    }

    public void deleteCorrespondence(@NonNull final int id, @NonNull final CallBack callback) {
        final String url = String.format("%s/api/correspondances/"+id, URL);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.DELETE, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","ERROR " + response);
                            callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        callback.onError(error);

                    }
                });
        queue.add(jsonObjectRequest);
    }

    public void cancel() {
        queue.cancelAll(CURRENT_TAG);
    }
}
