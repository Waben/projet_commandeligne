package com.example.project2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class activity_company_page extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_page);



        final Button button = findViewById(R.id.button4);
        button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {

                final Intent intent = new Intent(getApplicationContext(), activity_adding_stuff.class);
                startActivityForResult(intent,1);
            }
        });

        final Button button2 = findViewById(R.id.button3);
        button2.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {

                final Intent intent = new Intent(getApplicationContext(), activity_offers_list.class);
                startActivityForResult(intent,1);
            }
        });

        final Button button3 = findViewById(R.id.button5);
        button3.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                final Intent intent = new Intent(getApplicationContext(), activity_orders_list.class);
                startActivityForResult(intent,1);
            }
        });

        final Button button4 = findViewById(R.id.button6);
        button4.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                final Intent intent = new Intent(getApplicationContext(), activity_template_choice.class);
                startActivityForResult(intent,1);
            }
        });



    }
}