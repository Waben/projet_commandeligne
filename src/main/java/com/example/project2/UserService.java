package com.example.project2;

import android.app.Activity;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;

import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserService {
    private static final String TAG = UserService.class.getSimpleName();
    /**
     * Carte réseau sans fil Connexion au réseau local* 1 : on prend adresse ipv4 à chaque fois qu'on change de reseau
     */
    //private static final String URL = "http://192.168.0.35:5000/"; // vitrolles
    private static final String URL = "http://192.168.137.1:5000/"; // cite u
    private static final String CURRENT_WEATHER_TAG = "CURRENT_WEATHER";

    private RequestQueue queue;

    public UserService(@NonNull final Activity activity) {
        queue = Volley.newRequestQueue(activity.getApplicationContext());
    }

    public interface UserCallback {
        @MainThread
        void onUser(@NonNull final List<User> user);

        @MainThread
        void onError(@Nullable Exception exception);
    }
/*
    public void getUser(@NonNull final String locationName, @NonNull final UserCallback callback) {
        final String url = String.format("%s/api/users", URL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("Tag","ERROR " + response);
                        /*final JSONObject currentWeatherJSONObject = new JSONObject(response);
                            final JSONArray weather = currentWeatherJSONObject.getJSONArray("weather");
                            final JSONObject weatherCondition = weather.getJSONObject(0);
                            final String locationName = currentWeatherJSONObject.getString("name");
                            final int conditionId = weatherCondition.getInt("id");
                            final String conditionName = weatherCondition.getString("main");
                            final double tempKelvin = currentWeatherJSONObject.getJSONObject("main").getDouble("temp");
                            final User currentWeather = new User(locationName, conditionId, conditionName, tempKelvin);
                            final JSONObject currentUserJSONObject = new JSONObject(response);
                            final JSONArray userJson = currentUserJSONObject.getJSONArray("users");
                            List<User > users = new ArrayList<>();
                            for (int i = 0; i < userJson.length() ; i++) {
                                final JSONObject premier = userJson.getJSONObject(i);
                                final int id = premier.getInt("id");
                                final String name = premier.getString("name");
                                User user = new User(id, name);
                                users.add(user);
                            }

                            callback.onUser(users);
                        } catch (Exception e) {
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error);
            }
        });
        stringRequest.setTag(CURRENT_WEATHER_TAG);
        queue.add(stringRequest);
    }*/
    public void getUser(@NonNull final String locationName, @NonNull final UserCallback callback) {
        final String url = String.format("%s/api/users", URL);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","ERROR " + response);
                        /*final JSONObject currentWeatherJSONObject = new JSONObject(response);
                            final JSONArray weather = currentWeatherJSONObject.getJSONArray("weather");
                            final JSONObject weatherCondition = weather.getJSONObject(0);
                            final String locationName = currentWeatherJSONObject.getString("name");
                            final int conditionId = weatherCondition.getInt("id");
                            final String conditionName = weatherCondition.getString("main");
                            final double tempKelvin = currentWeatherJSONObject.getJSONObject("main").getDouble("temp");
                            final User currentWeather = new User(locationName, conditionId, conditionName, tempKelvin);*/
                            //final JSONObject currentUserJSONObject = new JSONObject(response);
                            final JSONArray userJson = response.getJSONArray("users");
                            List<User > users = new ArrayList<>();
                            for (int i = 0; i < userJson.length() ; i++) {
                                final JSONObject premier = userJson.getJSONObject(i);
                                final int id = premier.getInt("id");
                                final String name = premier.getString("name");
                                User user = new User(id, name);
                                users.add(user);
                            }

                            callback.onUser(users);
                        } catch (Exception e) {
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        callback.onError(error);

                    }
                });
        queue.add(jsonObjectRequest);
    }

    public void postUser( @NonNull final UserCallback callback) {
        //a modifier
        final String url = String.format("%s/api/users", URL);
        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("name", "blabla");
        Log.d(TAG,"Json:"+ new JSONObject(jsonParams));
        //StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, new JSONObject(jsonParams), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","ERROR " + response);
                        /*final JSONObject currentWeatherJSONObject = new JSONObject(response);
                            final JSONArray weather = currentWeatherJSONObject.getJSONArray("weather");
                            final JSONObject weatherCondition = weather.getJSONObject(0);
                            final String locationName = currentWeatherJSONObject.getString("name");
                            final int conditionId = weatherCondition.getInt("id");
                            final String conditionName = weatherCondition.getString("main");
                            final double tempKelvin = currentWeatherJSONObject.getJSONObject("main").getDouble("temp");
                            final User currentWeather = new User(locationName, conditionId, conditionName, tempKelvin);*/
                            //final JSONObject currentUserJSONObject = new JSONObject(response);
                            //final JSONArray userJson = response.getJSONArray("users");
                            List<User > users = new ArrayList<>();
                            /*
                            for (int i = 0; i < userJson.length() ; i++) {
                                final JSONObject premier = userJson.getJSONObject(i);
                                final int id = premier.getInt("id");
                                final String name = premier.getString("name");
                                User user = new User(id, name);
                                users.add(user);
                            }*/
                            callback.onUser(users);
                        } catch (Exception e) {
                            System.out.println("error try");
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String,String>();
                 headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        //stringRequest.setTag(CURRENT_WEATHER_TAG);
        queue.add(jsonObjectRequest);
    }

    public void putUser( @NonNull final UserCallback callback, @NonNull int id) {
        //a modifier
        final String url = String.format("%s/api/users/" + id, URL);
        JSONObject jsonParams = new JSONObject();
        try{
            jsonParams.put("id", id);
            jsonParams.put("name", "bloblo");
        }catch (JSONException e){
            e.printStackTrace();
        }

        Log.d("BEFORE","Json:"+ jsonParams);
        //StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.PUT, url, jsonParams, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","ERROR " + response);
                        /*final JSONObject currentWeatherJSONObject = new JSONObject(response);
                            final JSONArray weather = currentWeatherJSONObject.getJSONArray("weather");
                            final JSONObject weatherCondition = weather.getJSONObject(0);
                            final String locationName = currentWeatherJSONObject.getString("name");
                            final int conditionId = weatherCondition.getInt("id");
                            final String conditionName = weatherCondition.getString("main");
                            final double tempKelvin = currentWeatherJSONObject.getJSONObject("main").getDouble("temp");
                            final User currentWeather = new User(locationName, conditionId, conditionName, tempKelvin);*/
                            //final JSONObject currentUserJSONObject = new JSONObject(response);
                            //final JSONArray userJson = response.getJSONArray("users");
                            List<User > users = new ArrayList<>();
                            /*
                            for (int i = 0; i < userJson.length() ; i++) {
                                final JSONObject premier = userJson.getJSONObject(i);
                                final int id = premier.getInt("id");
                                final String name = premier.getString("name");
                                User user = new User(id, name);
                                users.add(user);
                            }*/
                            callback.onUser(users);
                        } catch (Exception e) {
                            System.out.println("error try");
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String,String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        //stringRequest.setTag(CURRENT_WEATHER_TAG);
        queue.add(jsonObjectRequest);
    }

    public void deleteUser(@NonNull final int id, @NonNull final UserCallback callback) {
        final String url = String.format("%s/api/users/"+id, URL);
        JSONObject jsonParams = new JSONObject();
        try{
            jsonParams.put("id", id);
            jsonParams.put("name", "blabla");
        }catch (JSONException e){
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.DELETE, url, jsonParams, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","ERROR " + response);
                        /*final JSONObject currentWeatherJSONObject = new JSONObject(response);
                            final JSONArray weather = currentWeatherJSONObject.getJSONArray("weather");
                            final JSONObject weatherCondition = weather.getJSONObject(0);
                            final String locationName = currentWeatherJSONObject.getString("name");
                            final int conditionId = weatherCondition.getInt("id");
                            final String conditionName = weatherCondition.getString("main");
                            final double tempKelvin = currentWeatherJSONObject.getJSONObject("main").getDouble("temp");
                            final User currentWeather = new User(locationName, conditionId, conditionName, tempKelvin);*/
                            //final JSONObject currentUserJSONObject = new JSONObject(response);
                            final JSONArray userJson = response.getJSONArray("users");
                            List<User > users = new ArrayList<>();
                            for (int i = 0; i < userJson.length() ; i++) {
                                final JSONObject premier = userJson.getJSONObject(i);
                                final int id = premier.getInt("id");
                                final String name = premier.getString("name");
                                User user = new User(id, name);
                                users.add(user);
                            }

                            callback.onUser(users);
                        } catch (Exception e) {
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        callback.onError(error);

                    }
                });
        queue.add(jsonObjectRequest);
    }

    public void cancel() {
        queue.cancelAll(CURRENT_WEATHER_TAG);
    }
}
