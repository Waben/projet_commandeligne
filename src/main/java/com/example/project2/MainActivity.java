package com.example.project2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), activity_template_choice.class);


               /* final City item = (City) parent.getItemAtPosition(position);
                intent.putExtra(City.TAG, item );*/


                startActivityForResult(intent,1);
            }
        });

        final TextView textView = findViewById(R.id.textView6);
        textView.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
               final Intent intent = new Intent(getApplicationContext(), activity_signup.class);


               /* final City item = (City) parent.getItemAtPosition(position);
                intent.putExtra(City.TAG, item );*/


                startActivityForResult(intent,1);
            }
        });


    }
}