package com.example.project2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.project2.tableClass.Offer;

import java.util.ArrayList;
import java.util.List;

public class activity_offers_list extends AppCompatActivity {
    class OfferList  extends BaseAdapter {
        List<Offer> dataSource;

        public OfferList(List<Offer> data) {
            //super(MainActivity.this, R.layout.row, data);
            dataSource=data;
        }
        public void setData(List<Offer> d)
        {
            dataSource=d;
        }
        @Override
        public int getCount() {
            return dataSource.size();
        }
        @Override
        public Object getItem(int position) {
            return dataSource.get(position);
        }
        @Override
        public long getItemId(int position) {
            return position;
        }
        public void updateCityList(List<Offer> newlist) {
            dataSource.clear();
            dataSource.addAll(newlist);
            this.notifyDataSetChanged();
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Offer c = dataSource.get(position);
            convertView = LayoutInflater.from(activity_offers_list.this).inflate(R.layout.fragment_offerlist_item,null);

            TextView Text = (TextView) convertView.findViewById(R.id.text1);

            String Name="Offer ";

            if(c.getOfferType()==1)
            {
                Name+=+c.getValue1()+"% on ";
                Name+="?";
                Text.setText(Name);
            }
            else
            {
                Name+="Get "+c.getValue1()+" Bying ";
                Name+=c.getValue2()+" on ";
                Name+="?";
                Text.setText(Name);
            }

            return convertView;
        }
    }
    OfferList OL=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offers_list);



       /* WeatherDbHelper dbHelper = new WeatherDbHelper(this);
        CL = new CityList(dbHelper.getAllCities());*/

        ArrayList<Offer> ArrOff=new ArrayList<>();
        ArrOff.add(new Offer(0, "Help", 1, 1, 1, 1));
        ArrOff.add(new Offer(0, "Help", 2, 1, 1, 1));
        ArrOff.add(new Offer(0, "Help", 3, 2, 1, 1));
        ArrOff.add(new Offer(0, "Help", 1, 75, 1, 1));
        OL=new OfferList(ArrOff);

        ListView list = (ListView) findViewById(R.id.listview);
        list.setAdapter(OL);


    }
}