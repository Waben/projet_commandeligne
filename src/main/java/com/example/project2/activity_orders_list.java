package com.example.project2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.project2.tableClass.Command;

import java.util.ArrayList;
import java.util.List;

public class activity_orders_list extends AppCompatActivity {
    class OrderList  extends BaseAdapter {
        List<Command> dataSource;

        public OrderList(List<Command> data) {
            //super(MainActivity.this, R.layout.row, data);
            dataSource=data;
        }
        public void setData(List<Command> d)
        {
            dataSource=d;
        }
        @Override
        public int getCount() {
            return dataSource.size();
        }
        @Override
        public Object getItem(int position) {
            return dataSource.get(position);
        }
        @Override
        public long getItemId(int position) {
            return position;
        }
        public void updateCityList(List<Command> newlist) {
            dataSource.clear();
            dataSource.addAll(newlist);
            this.notifyDataSetChanged();
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Command c = dataSource.get(position);
            convertView = LayoutInflater.from(activity_orders_list.this).inflate(R.layout.fragment_commandlist_item,null);

            TextView Text1 = (TextView) convertView.findViewById(R.id.textView5);
            TextView Text2 = (TextView) convertView.findViewById(R.id.textView7);
            TextView Text3 = (TextView) convertView.findViewById(R.id.textView8);
            TextView Text4 = (TextView) convertView.findViewById(R.id.textView9);

            Text1.setText("Command "+position);
            Text2.setText("Monbec");
            Text3.setText("Tesseracte");
            Text4.setText("972 Avenue Joseph Staline");

            return convertView;
        }
    }
    OrderList OL=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_list);




        ArrayList<Command> ArrOff=new ArrayList<>();
        ArrOff.add(new Command(0, "Help", 1, "", 1, 1));
        ArrOff.add(new Command(0, "Help", 2, "1", 1, 1));
        ArrOff.add(new Command(0, "Help", 3, "2", 1, 1));
        ArrOff.add(new Command(0, "Help", 1, "75", 1, 1));

        ArrOff.add(new Command(0, "Help", 1, "", 1, 1));
        ArrOff.add(new Command(0, "Help", 2, "1", 1, 1));
        ArrOff.add(new Command(0, "Help", 3, "2", 1, 1));
        ArrOff.add(new Command(0, "Help", 1, "75", 1, 1));

        ArrOff.add(new Command(0, "Help", 1, "", 1, 1));
        ArrOff.add(new Command(0, "Help", 2, "1", 1, 1));
        ArrOff.add(new Command(0, "Help", 3, "2", 1, 1));
        ArrOff.add(new Command(0, "Help", 1, "75", 1, 1));

        ArrOff.add(new Command(0, "Help", 1, "", 1, 1));
        ArrOff.add(new Command(0, "Help", 2, "1", 1, 1));
        ArrOff.add(new Command(0, "Help", 3, "2", 1, 1));
        ArrOff.add(new Command(0, "Help", 1, "75", 1, 1));

        ArrOff.add(new Command(0, "Help", 1, "", 1, 1));
        ArrOff.add(new Command(0, "Help", 2, "1", 1, 1));
        ArrOff.add(new Command(0, "Help", 3, "2", 1, 1));
        ArrOff.add(new Command(0, "Help", 1, "75", 1, 1));
        OL=new OrderList(ArrOff);

        ListView list = (ListView) findViewById(R.id.listview);
        list.setAdapter(OL);


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id)
            {

                final Intent intent = new Intent(getApplicationContext(), activity_command.class);
                final com.example.project2.tableClass.Command item = (com.example.project2.tableClass.Command) parent.getItemAtPosition(position);
                intent.putExtra(com.example.project2.tableClass.Command.TAG, item );
                startActivityForResult(intent,1);
            }
        });

    }
}