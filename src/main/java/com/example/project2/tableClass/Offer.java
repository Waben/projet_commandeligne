package com.example.project2.tableClass;

import com.google.android.material.tabs.TabLayout;

public class Offer extends Table {

    String name;
    int offerType;
    int value1;
    int value2;
    int shopId;

    public Offer(int id, String name, int offerType, int value1, int value2, int shopId) {
        super(id);
        this.name = name;
        this.offerType = offerType;
        this.value1 = value1;
        this.value2 = value2;
        this.shopId = shopId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOfferType() {
        return offerType;
    }

    public void setOfferType(int offerType) {
        this.offerType = offerType;
    }

    public int getValue1() {
        return value1;
    }

    public void setValue1(int value1) {
        this.value1 = value1;
    }

    public int getValue2() {
        return value2;
    }

    public void setValue2(int value2) {
        this.value2 = value2;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    @Override
    public String toString() {
        return "Offer{" +
                "name='" + name + '\'' +
                ", offerType=" + offerType +
                ", value1=" + value1 +
                ", value2=" + value2 +
                ", shopId=" + shopId +
                ", id=" + id +
                '}';
    }
}
