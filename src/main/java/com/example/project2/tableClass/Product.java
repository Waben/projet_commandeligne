package com.example.project2.tableClass;

public class Product extends Table{

    String name;
    String status;
    int price;
    int offerId;
    int shopId;
    int categoryId;

    public Product(int id, String name, String status, int price, int offerId, int shopId, int categoryId) {
        super(id);
        this.name = name;
        this.status = status;
        this.price = price;
        this.offerId = offerId;
        this.shopId = shopId;
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getOfferId() {
        return offerId;
    }

    public void setOfferId(int offerId) {
        this.offerId = offerId;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", status='" + status + '\'' +
                ", price=" + price +
                ", offerId=" + offerId +
                ", shopId=" + shopId +
                ", categoryId=" + categoryId +
                ", id=" + id +
                '}';
    }
}
