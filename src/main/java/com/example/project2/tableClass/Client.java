package com.example.project2.tableClass;

public class Client extends Table {

    String name;
    String surname;
    String mail;
    String addresse;
    int idBasket;
    int idHobby;

    public Client(int id, String name, String surname, String mail, String addresse, int idBasket, int idHobby) {
        super(id);
        this.name = name;
        this.surname = surname;
        this.mail = mail;
        this.addresse = addresse;
        this.idBasket = idBasket;
        this.idHobby = idHobby;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getAddresse() {
        return addresse;
    }

    public void setAddresse(String addresse) {
        this.addresse = addresse;
    }

    public int getIdBasket() {
        return idBasket;
    }

    public void setIdBasket(int idBasket) {
        this.idBasket = idBasket;
    }

    public int getIdHobby() {
        return idHobby;
    }

    public void setIdHobby(int idHobby) {
        this.idHobby = idHobby;
    }

    @Override
    public String toString() {
        return "Client{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", mail='" + mail + '\'' +
                ", addresse='" + addresse + '\'' +
                ", idBasket=" + idBasket +
                ", idHobby=" + idHobby +
                ", id=" + id +
                '}';
    }
}
