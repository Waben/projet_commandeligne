package com.example.project2.tableClass;

public class CommandList extends Table {

    int quantity;

    int commandId;

    int shopId;

    public CommandList(int id, int quantity, int commandId, int shopId) {
        super(id);
        this.quantity = quantity;
        this.commandId = commandId;
        this.shopId = shopId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getCommandId() {
        return commandId;
    }

    public void setCommandId(int commandId) {
        this.commandId = commandId;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    @Override
    public String toString() {
        return "CommandList{" +
                "quantity=" + quantity +
                ", commandId=" + commandId +
                ", shopId=" + shopId +
                ", id=" + id +
                '}';
    }
}
