package com.example.project2.tableClass;

public class ProductPicture extends Table {

    String name;
    String location;
    int productId;
    int productTypeId;

    public ProductPicture(int id, String name, String location, int productId, int productTypeId) {
        super(id);
        this.name = name;
        this.location = location;
        this.productId = productId;
        this.productTypeId = productTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(int productTypeId) {
        this.productTypeId = productTypeId;
    }

    @Override
    public String toString() {
        return "ProductPicture{" +
                "name='" + name + '\'' +
                ", location='" + location + '\'' +
                ", productId=" + productId +
                ", productTypeId=" + productTypeId +
                ", id=" + id +
                '}';
    }
}
