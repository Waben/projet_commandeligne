package com.example.project2.tableClass;

public class Table {
    int id;

    public Table(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Table{" +
                "id=" + id +
                '}';
    }
}
